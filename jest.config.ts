import type { JestConfigWithTsJest } from 'ts-jest';
const jestConfig: JestConfigWithTsJest = {
  collectCoverage: true,
  collectCoverageFrom: [
    '**/src/**/*.{js,jsx,ts}',
    '!**/src/**/reports/*.{js,jsx,ts}',
    '!**/node_modules/**',
    '!**/vendor/**',
  ],
  preset: 'ts-jest',
  testEnvironment: 'node',
  extensionsToTreatAsEsm: ['.ts'],
  moduleNameMapper: {
    '^(\\.{1,2}/.*)\\.js$': '$1',
  },
  testMatch: [
    '**/test/**/*.[jt]s'
  ],
  transform: {
    '^.+\\.[tj]sx?$': [
      'ts-jest',
      {
        useESM: true,
      },
    ],
  },
  reporters: [['summary', {summaryThreshold: 1}]]
}

export default jestConfig;