/** ***************************************************************
 Copyright (C) 2022 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/

import yargs from 'yargs';
const args = yargs(process.argv)
  .usage('Usage: $0 [options]')
  .example('$0', '')
  .option('H', {
    alias: 'host',
    description: 'GitLab host base URL',
    default: 'http://gitlab.eclipse.org/',
    string: true,
  })
  .option('s', {
    alias: 'secretLocation',
    description: 'The location of the access-token file containing an API access token',
    string: true,
  }).argv;

import { Logger } from 'winston';
import { getLogger } from '../../helpers/logger';
import { GitlabSyncRunner } from './GitlabSyncRunner';

// used to split project name from group namespace
const PROJECT_SPLIT_REGEX = /^(.*)\/([^/]+)$/gm;

// These values are pulled from the AccessLevel type, as it isn't typesafe otherwise
// eslint-disable-next-line no-magic-numbers
const accessLevelComparable: ReadonlyArray<number> = [0, 5, 10, 20, 30, 40, 50] as const;
const isAccessLevel = (x: unknown): x is NonAdminAccessLevel => {
  if (!x || typeof x !== 'number') {
    return false;
  }
  return accessLevelComparable.includes(x);
};

// simple sample project/group structure to create
enum SampleType {
  project = 'project',
  group = 'group',
}
interface EclipseUserAccess {
  url: string;
  accessLevel: number;
}
interface SampleStructure {
  namespace: string;
  users: Record<string, EclipseUserAccess>;
  type: SampleType;
}
import * as projectsRaw from './test_environment.json';
import { NonAdminAccessLevel } from './GitlabWrapper';
import { SimpleUserSchema } from '@gitbeaker/rest';
const sampleStructures = <SampleStructure[]>projectsRaw;

export class TestEnvironment {
  syncRunner: GitlabSyncRunner;
  logger: Logger;

  constructor({ secretLocation, host }: Record<string, string>) {
    this.syncRunner = new GitlabSyncRunner({
      devMode: false,
      host: host,
      dryRun: false,
      provider: 'oauth2_generic',
      verbose: true,
      project: null,
      user: 'webmaster',
      secretLocation: secretLocation,
      staging: false,
    });
    this.logger = getLogger('debug', 'main');
  }

  /**
   * Run the test environment build script.
   */
  async run(): Promise<void> {
    await this.syncRunner.prepareCaches();
    // ensure the needed root group exists
    await this.createRootGroup('eclipse');
    for (const sampleStructureIdx in sampleStructures) {
      const sampleStructure = sampleStructures[sampleStructureIdx];
      switch (sampleStructure.type) {
        case SampleType.group:
          await this.handleSampleGroup(sampleStructure);
          break;
        case SampleType.project:
          await this.handleSampleProject(sampleStructure);
          break;
      }
    }
  }

  /**
   * Builds the sample group with users set in the sample structure element.
   *
   * @param sampleStructure the group structure to be created.
   * @returns an async process for adding the group.
   */
  async handleSampleGroup(sampleStructure: SampleStructure): Promise<void> {
    // get the actual group
    const projectGroup = await this.syncRunner.getGroup(sampleStructure.namespace);
    if (projectGroup === null || projectGroup._self === null) {
      this.logger.error(`Could not find group with namespace ${projectGroup}`);
      return;
    }
    // Auto format breaks this line, so added rule ignore
    // eslint-disable-next-line space-before-function-paren
    await this.userAction(sampleStructure.users, async (user, accessLevel) => {
      this.logger.verbose(`Adding user ${user.id} to group ${projectGroup._self.path}`);
      await this.syncRunner.addUserToAsset(user, projectGroup._self, accessLevel);
    });
  }

  async handleSampleProject(sampleStructure: SampleStructure): Promise<void> {
    // get the matching project if it exists
    const matches = this.syncRunner.gitlabWrapper.projectsCache.find(v =>
      v.path_with_namespace.localeCompare(sampleStructure.namespace) !== 0);
    let projectId: number;
    if (!matches) {
      this.logger.info(`No match found for ${sampleStructure.namespace}, creating new project`);
      try {
        // create project, splitting the designated namespace into parts
        const results = PROJECT_SPLIT_REGEX.exec(sampleStructure.namespace);
        if (!results || !results[1] || !results[2]) {
          this.logger.error(`Could not extract a group from project namespace, skipping '${sampleStructure.namespace}'`);
          return;
        }
        const project = await this.syncRunner.gitlabWrapper.api.Projects.create({
          namespaceId: (await this.syncRunner.getGroup(results[1]))._self.id,
          name: results[2],
        });
        projectId = project.id;
      } catch (e) {
        this.logger.error(`Error while creating project '${sampleStructure.namespace}': ${e}`);
        return;
      }
    } else {
      projectId = matches.id;
    }
    if (projectId === undefined) {
      this.logger.error('no set project ID, skipping');
      return;
    }
    // Auto format breaks this line, so added rule ignore
    // eslint-disable-next-line space-before-function-paren
    this.userAction(sampleStructure.users, async (user, accessLevel) => {
      this.logger.verbose(`Adding user ${user.id} to project ${projectId}`);
      await this.syncRunner.gitlabWrapper.api.ProjectMembers.add(projectId, user.id, accessLevel);
    });
  }

  /**
   * Performs a user action for each user present in the users record. Will convert the user records to a UserSchema from Gitlab
   * and then perform the callback, along with the users access level.
   *
   * @param users the users to action on using the given callback
   * @param callback the action to perform for each user
   */
  async userAction(users: Record<string, EclipseUserAccess>, callback: (user: SimpleUserSchema, accessLevel: NonAdminAccessLevel)
    => Promise<void>) {
    const usernames = Object.keys(users);
    for (const usernameIdx in usernames) {
      const uname = usernames[usernameIdx];
      const user = await this.syncRunner.getUser(uname, users[uname].url);
      if (user === null) {
        this.logger.verbose(`Could not retrieve user for UID '${uname}', skipping`);
        continue;
      }
      // type check the access level
      const accessLevel = users[uname].accessLevel;
      if (!isAccessLevel(accessLevel)) {
        this.logger.warn(`Invalid access level of '${accessLevel}' passed, skipping adding user to entity`);
        continue;
      }
      // call the user action with the cast access level
      try {
        await callback(user, accessLevel);
      } catch (e) {
        this.logger.warn(`Error encountered during user action callback, some users may be out of sync: ${e}`);
      }
    }
  }

  /**
   * Creates root group (no parent group) in the target Gitlab instance.
   *
   * @param name the path and name of the Gitlab root level group to create.
   * @returns the group if it can be created
   */
  async createRootGroup(name: string): Promise<void> {
    try {
      const rootGroup = this.syncRunner.getRootGroup(false);
      if (rootGroup !== null && rootGroup._self !== null) {
        this.logger.info(`Root group '${name}' already exists, not creating group`);
        return;
      }
    } catch (e) {
      this.logger.info(`Creating root group '${name}'`);
    }
    try {
      await this.syncRunner.gitlabWrapper.api.Groups.create(name, name, {
        projectCreationLevel: 'maintainer',
        visibility: 'public',
        requestAccessEnabled: false,
      });
    } catch (e) {
      this.logger.error(`Could not create root group with name '${name}'`);
      process.exit(1);
    }
  }
}
// run the test environment creation script
new TestEnvironment({ secretLocation: args['s'], host: args['h'] }).run();
