/* eslint-disable comma-dangle */
/** ***************************************************************
 Copyright (C) 2022 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/

import { GroupSchema, IssueSchema, MemberSchema, ProjectSchema, SimpleUserSchema } from '@gitbeaker/rest';
import { v4 } from 'uuid';
import { Logger } from 'winston';
import { EclipseAPI, EclipseApiConfig } from '../../eclipse/EclipseAPI';
import { SecretReader, getBaseConfig, DEFAULT_SECRET_LOCATION } from '../../helpers/SecretReader';
import { getLogger } from '../../helpers/logger';
import { EclipseProject, InterestGroup } from '../../interfaces/EclipseApi';
import { GitlabWrapper, NonAdminAccessLevel, ProjectVisibility } from './GitlabWrapper';
import { UserPermissionsOverride } from '../../teams/UserPermissionsOverride';

const adminPermissionsLevel = 50;
const maintainerPermissionsLevel = 40;
const reporterPermissionsLevel: NonAdminAccessLevel = 20;
const allowlistedUsers: string[] = ['webmaster', 'root'];

/**
 * Used for private repo expiry
 */
const repoExpiryDays = 90;
const hoursInDay = 24;
const minsInHour = 60;
const millisInMinute = 60000;
const repoExpiryAsMillis = repoExpiryDays * hoursInDay * minsInHour * millisInMinute;

/**
 * Represents the nested group cache that can represent the relationships between groups and to simplify child lookups.
 */
interface GroupCache {
  _self: GroupSchema | null;
  projectTargets: string[];
  children: Record<string, GroupCache>;
}

export interface EclipseUserAccess {
  username: string;
  url: string;
  accessLevel: NonAdminAccessLevel;
  projectNamespace: string;
}

export interface GitlabSyncRunnerConfig {
  host: string;
  provider: string;
  secretLocation?: string;
  project?: string;
  verbose: boolean;
  devMode: boolean;
  dryRun: boolean;
  user: string,
  rootGroup?: string;
  staging?: boolean;
}

export class GitlabSyncRunner {
  // internal state
  accessToken = '';
  eclipseToken = '';
  config: GitlabSyncRunnerConfig;
  logger: Logger;
  // needs to be any as it currently uses JS instead of TS sources
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  upo: any;

  // api access
  gitlabWrapper: GitlabWrapper;
  eApi: EclipseAPI;

  // caches to optimize calling
  nestedGroupCache: GroupCache = {
    _self: null,
    children: {},
    projectTargets: [],
  };
  eclipseProjectCache: Record<string, EclipseProject> = {};
  eclipseIGProjectCache: Record<string, EclipseProject> = {};
  // added as a shortcut for when we want to generally check all projects
  combinedProjectsCache: Record<string, EclipseProject> = {};
  botsCache: Record<string, string[]> = {};

  /**
   * Sets the internal config with a few default values and creates the bindings for the APIs that are
   * accessed during the run of this script.
   *
   * @param config the initial script configuration object.
   */
  constructor(config: GitlabSyncRunnerConfig) {
    this.config = {
      host: 'http://gitlab.eclipse.org/',
      provider: 'oauth2_generic',
      verbose: false,
      devMode: false,
      dryRun: false,
      user: 'webmaster',
      rootGroup: 'eclipse',
      ...config
    };

    this.logger = getLogger(this.config.verbose ? 'debug' : 'info', 'main');
    this.logger.info(`Running with configs: ${JSON.stringify(this.config)}`);
    this._prepareSecret();

    // create API instances
    this.gitlabWrapper = new GitlabWrapper({
      host: this.config.host,
      token: this.accessToken,
      verbose: this.config.verbose,
      user: this.config.user,
      rootGroup: this.config.rootGroup,
    });
    const eclipseAPIConfig: EclipseApiConfig = JSON.parse(this.eclipseToken);
    eclipseAPIConfig.testMode = this.config.devMode;
    eclipseAPIConfig.verbose = this.config.verbose;
    this.eApi = new EclipseAPI(eclipseAPIConfig);
    // wrap the override creation to catch errors before exiting
    try {
      this.upo = new UserPermissionsOverride(`${this.config.secretLocation || DEFAULT_SECRET_LOCATION}/permissions.json`);
    } catch (e) {
      this.logger.error(`Error while attempting to load user permission overrides, cannot continue: ${e}`);
      process.exit(1);
    }
  }

  /**
   * Prepares the secrets required for the script to run. Specifically the eclipseToken used for Eclipse
   * API access and the accessToken which is used for sudo+api access on the Gitlab instance targeted by
   * this script.
   */
  _prepareSecret() {
    // retrieve the secret API file root if set
    const settings = getBaseConfig();
    if (this.config.secretLocation !== undefined) {
      settings.root = this.config.secretLocation;
    }
    const reader = new SecretReader(settings);
    let data = reader.readSecret('access-token');
    if (data !== null) {
      this.accessToken = data.trim();
      // retrieve the Eclipse API token (needed for emails)
      data = reader.readSecret('eclipse-oauth-config');
      if (data !== null) {
        this.eclipseToken = data.trim();
      } else {
        this.logger.error('Could not find the Eclipse OAuth config, exiting');
        process.exit(1);
      }
    } else {
      this.logger.error('Could not find the GitLab access token, exiting');
      process.exit(1);
    }
  }

  /**
   * Run the full sync script, syncing the PMI to the Gitlab instance targeted by the script. This script
   * will sync the namespace groups named in projects with the users that are set as members of the project.
   * It will also clear users added outside of this process with the exception of bot users to maintain more
   * strict control of the access permissions.
   *
   * @returns a promise that is completed once the run completes.
   */
  async run(): Promise<void> {
    // prepopulate caches to optimally retrieve info used in sync ops
    await this.gitlabWrapper.prepareCaches();
    await this.prepareCaches();
    // fetch org group from results, create if missing
    this.logger.info('Starting sync');
    const g = this.getRootGroup();
    if (g._self === null) {
      this.logger.error(`Unable to start sync of GitLab content. Base group (${this.config.rootGroup}) could not be found`);
      return;
    }
    // perform base project sync
    await this.performEclipseProjectSync(this.eclipseProjectCache);
    // perform IG syncing
    await this.performIGSync();

    // perform cleanup operations to clean out extra users and flag private repos for deletion
    await this.cleanupGroups();
    await this.cleanupProjects();
    await this.managePrivateProjects();
  }

  /**
   * Syncs over a mapping of Eclipse projects to the Gitlab instance, using the passed rootGroup as the Gitlab group to use as the root of
   * sync'd projects and groups. Generates permission sets and imports users as needed to fulfill project, cleaning up users that were
   * either retired or were added outside of the approved process.
   *
   * @param projectMapping mapping of EF projects to their project ID to the project object
   * @param rootGroup the name of the group to use as the root of EF imports.
   */
  async performEclipseProjectSync(projectMapping: Record<string, EclipseProject>, rootGroup: string = this.config.rootGroup) {
    for (const projectIdx in projectMapping) {
      const project = projectMapping[projectIdx];
      if (this.config.project !== undefined && project.short_project_id !== this.config.project) {
        this.logger.info(`Project target set ('${this.config.project}'). Skipping non-matching project ID ${project.short_project_id}`);
        continue;
      }
      this.logger.info(`Processing ${project.project_id}`);

      // sync the security team for the project, do this early as we always want to sync, even for non-GL projects
      await this.performProjectSyncForSecurity(project);

      // fetch group namespace indicated by the project and ensure format
      const actualNamespace = project.gitlab.project_group;
      const [projectNamespace, projectNamespaceTLP] = [
        `${rootGroup}/${project.short_project_id}`,
        `${rootGroup}/${project.top_level_project}/${project.short_project_id}`,
      ];
      if (actualNamespace === undefined || actualNamespace.trim() === '') {
        this.logger.info(`Skipping project '${project.project_id}' as it has no Gitlab namespace`);
        continue;
      } else if (
        actualNamespace.localeCompare(projectNamespace, undefined, { sensitivity: 'base' }) !== 0 &&
        actualNamespace.localeCompare(projectNamespaceTLP, undefined, { sensitivity: 'base' }) !== 0
      ) {
        this.logger.info(
          `Skipping namespace '${actualNamespace}' for project '${project.short_project_id}', does not match allowed formats`
        );
        continue;
      }
      this.logger.info(`Processing '${project.short_project_id}'`);

      // check group cache to ensure well formed.
      const namespaceGroup = await this.getGroup(actualNamespace, rootGroup);
      if (namespaceGroup === null || namespaceGroup._self === null) {
        this.logger.error(`Could not find group with namespace ${actualNamespace}`);
        continue;
      }
      // update the group to add the users for the current project
      await this.handleProjectUsers(project, this.getUserList(project), namespaceGroup);

      // retrieve bots for current project and add them to the groups
      for (const botIdx in this.botsCache[project.project_id]) {
        const bot = this.botsCache[project.project_id][botIdx];
        this.logger.verbose(`Found ${bot} for ${project.project_id}`);
        // get the bot user if it exists already
        const botUser = await this.getUser(bot, bot);
        if (botUser == null) {
          this.logger.info(
            `Could not retrieve user for bot user ${bot} for project ${project.project_id}, ` + 'not attempting to add to group'
          );
          continue;
        }
        // add bot user to the group
        this.logger.verbose(`Adding bot ${bot} to group ${namespaceGroup._self.path}`);
        await this.addUserToAsset(botUser, namespaceGroup._self, adminPermissionsLevel);
      }
    }
  }

  /**
   * Check to see if a security project exists for an Eclipse project, and if so, sync the users to the project if any are set.
   *
   * @param project the Eclipse project to process for security team fields
   */
  async performProjectSyncForSecurity(project: EclipseProject) {
    // get security projects with path like /security/:projectId
    const securityProjects = this.gitlabWrapper.getAllProjects().filter(p => p.path_with_namespace.localeCompare(
      `security/${project.project_id}`, undefined, { sensitivity: 'base' }) === 0);
    if (securityProjects.length === 0) {
      this.logger.info(`Project '${project.project_id}' has no discoverable security project, will not sync security team`);
      return;
    }
    const securityProject = securityProjects[0];
    this.logger.info(`Found security project for ${project.project_id} at 'security/${project.project_id}', ID ${securityProject.id}`);

    // get the users for the security project
    let members = [...project.security_team.individual_members];
    // add members from other teams if flagged
    if (project.security_team.groups.include_committers) {
      members.push(...project.committers);
    }
    if (project.security_team.groups.include_project_leads) {
      members.push(...project.project_leads);
    }
    // deduplicate the list and set back
    members = members.filter((p1, i, a) => a.findIndex(p2 => p2.username.localeCompare(p1.username, undefined, { sensitivity: 'base' })
      === 0) === i);

    // add users to the security project
    for (const userRef of members) {
      const user = await this.getUser(userRef.username, userRef.url);
      if (user == null) {
        this.logger.verbose(`Could not retrieve user for UID '${userRef.username}', skipping`);
        continue;
      }
      // add users as the "reporter" level to the project directly
      await this.addUserToAsset(user, securityProject, reporterPermissionsLevel);
    }

    // map the user refs to an access map to remove additional users
    const expectedMembers: Array<EclipseUserAccess> = [];
    members.forEach(user => {
      expectedMembers.push({
        username: user.username,
        url: user.url,
        accessLevel: reporterPermissionsLevel,
        projectNamespace: securityProject.path_with_namespace
      });
    });
    // remove extra users from the project
    this.removeAdditionalUsers(expectedMembers, securityProject, project.project_id);
  }

  /**
   * Handles adding users from a proejct to the projects namespace grouping with the current assigned permissions.
   *
   * @param project the current Eclipse project
   * @param projectUserMap mapping of project users to their permissions within the project
   * @param namespaceGroup the Gitlab group that is being updated for the current project
   */
  async handleProjectUsers(project: EclipseProject, projectUserMap: Array<EclipseUserAccess>, namespaceGroup: GroupCache) {
    for (const access of projectUserMap) {
      const user = await this.getUser(access.username, access.url);
      if (user == null) {
        this.logger.verbose(`Could not retrieve user for UID '${access.username}', skipping`);
        continue;
      }

      await this.addUserToAsset(user, namespaceGroup._self, access.accessLevel);
      // if not tracked, track current project for group for post-sync cleanup
      if (namespaceGroup.projectTargets.indexOf(project.project_id) === -1) {
        namespaceGroup.projectTargets.push(project.project_id);
      }
    }
  }

  async performIGSync(rootGroup = 'eclipse-ig') {
    this.logger.debug('performIGSync()');
    if (Object.keys(this.eclipseIGProjectCache).length > 0) {
      await this.performEclipseProjectSync(this.eclipseIGProjectCache, rootGroup);
    }
  }

  /**
   * Generates the caches needed for running the Gitlab sync process.
   */
  async prepareCaches() {
    // get raw project data and post process to add additional context
    try {
      this.logger.info('Populating projects cache');
      const data = await this.eApi.eclipseAPI();

      const interestGroups = await this.eApi.interestGroups();
      this.logger.info(`Converting ${interestGroups.length} interest groups into projects for sync.`);

      this.eclipseIGProjectCache = this.adaptInterestGroupsToProject(interestGroups)
        .filter(ep => ep.project_id.length > 0)
        .reduce((acc, item) => ({ ...acc, [item.project_id]: item }), {} as Record<string, EclipseProject>);
      this.logger.info(`Adapted interest groups into ${Object.keys(this.eclipseIGProjectCache).length} projects after applied filters`);

      this.logger.info('Loading Eclipse bots');
      // get the bots for the projects
      this.logger.info('Populating bots cache');
      const rawBots = await this.eApi.eclipseBots();
      this.botsCache = this.eApi.processBots(rawBots, 'gitlab.eclipse.org');

      this.logger.info('Populating Gitlab groups cache');
      const groups = this.gitlabWrapper.getAllGroups();

      this.logger.info('Processing Gitlab groups for cache');
      // generates the nested cache
      this.generateGroupsCache(groups);

      this.logger.info('Mapping Gitlab projects');
      this.eclipseProjectCache = data.reduce((acc, item) => ({ ...acc, [item.project_id]: item }), {} as Record<string, EclipseProject>);
      this.combinedProjectsCache = {...this.eclipseProjectCache, ...this.eclipseIGProjectCache};
    } catch (e) {
      this.logger.error(e);
      this.logger.error(`Cannot fetch resources associated with sync operations, exiting: ${e}`);
      process.exit(1);
    }
  }

  /**
   * Iterate through each group, checking self and ancestor project users and comparing against the current groups users to ensure that
   * there are no additional users added with permissions.
   */
  async cleanupGroups(currentLevel: GroupCache = this.getRootGroup(), collectedProjects: string[] = []) {
    this.logger.debug(`cleanupGroups(currentLevel = ${currentLevel._self?.full_path}, collectedProjects = ${collectedProjects})`);
    const self = currentLevel._self;
    if (self === null) {
      this.logger.error('Error encountered during group cleanup process, ending early');
      return;
    }
    // collect and deduplicate project IDs
    const projects = [...Array.from(new Set([...currentLevel.projectTargets, ...collectedProjects]))];
    // build the user mapping to pass to cleanup
    let projectUsers: Array<EclipseUserAccess> = [];
    for (const pidx in projects) {
      projectUsers = [...projectUsers, ...this.getUserList(this.combinedProjectsCache[projects[pidx]])];
    }
    // clean up additional users
    await this.removeAdditionalUsers(projectUsers, self, ...projects);
    // for each of the children, pass the collected projects forward and process
    for (const cidx in currentLevel.children) {
      await this.cleanupGroups(currentLevel.children[cidx], projects);
    }
  }

  /**
   * Removes users not tracked in the expectedUsers map from the passed asset, either group or project. Eclipse Project IDs are used to
   * look up bot user account names as they are exempt from being removed as they are used for CI ops.
   *
   * @param expectedUsers map of usernames to their EclipseUser entry
   * @param asset the Gitlab asset that is being cleaned of extra users, either a group or project.
   * @param projectIDs list of project IDs that impact the given Gitlab asset
   * @returns a promise that completes once all additional users are removed or the check finishes
   */
  async removeAdditionalUsers(
    expectedUsers: Array<EclipseUserAccess>,
    asset: GroupSchema | ProjectSchema,
    ...projectIDs: string[]
  ): Promise<void> {
    if (this.config.verbose) {
      this.logger.debug(
        `GitlabSync:removeAdditionalUsers(expectedUsers = ${JSON.stringify(expectedUsers)}, asset = ${asset.full_path
        }, projectIDs = ${projectIDs})`
      );
    }
    const isGroup = this.isGroup(asset);
    // get the current list of users for the group
    const members = await (isGroup ? this.getGroupMembers(asset) : this.gitlabWrapper.getAllProjectMembers(asset.id, false));
    if (members === null || !(members instanceof Array)) {
      this.logger.warn(`Could not find any members for asset '${asset.full_path}'. Skipping user removal check`);
      return;
    }
    // check that each of the users in the group match whats expected
    members?.forEach(async member => {
      // check access and ensure user isn't an owner
      this.logger.verbose(`Checking user '${member.username}' access to asset '${asset.name}'`);
      if (this.shouldRemoveUser(member, expectedUsers, projectIDs)) {
        if (this.config.dryRun) {
          this.logger.info(`Dryrun flag active, would have removed user '${member.username}' from asset '${asset.name}'`);
          return;
        }
        this.logger.info(`Removing user '${member.username}' from asset '${asset.name}'`);

        await (isGroup ? this.gitlabWrapper.removeGroupMember(asset.id, member.id)
          : this.gitlabWrapper.removeProjectMember(asset.id, member.id))
          .catch(err => {
            if (this.config.verbose) {
              this.logger.error(`${err}`);
            }
            this.logger.warn(`Error while removing user '${member.username}' from asset '${asset.name}'`);
          });
      }
    });
  }

  /**
   * Checks for the following states:
   *
   * - User is outside the allowlisted users
   * - User is outside the expected user list
   * - The user has the wrong permissions set and isn't a project lead
   * - the user isn't a bot
   *
   * @param member the current group member being checked
   * @param expectedUsers the user access mapping for the current group
   * @param projectIDs projects associated with the current group
   * @param expectedUsernames the usernames from the users mapping, passed to save processing time
   * @returns true if all conditions in method description are met, otherwise false
   */
  shouldRemoveUser(
    member: MemberSchema,
    expectedUsers: Array<EclipseUserAccess>,
    projectIDs: string[]
  ): boolean {
    // fix helpdesk#5421 - use locale compare for string compare, as it can handle case insensitive compares
    // fix helpdesk#5073 - instead of replacing perms, we need to keep all and just find highest valid (inherited perms in GL)
    // get access objects for given user, then sort descending on access level (highest access on top in case of nested)
    const accessForUser = expectedUsers.filter(userAccess => userAccess.username.localeCompare(member.username, undefined,
      { sensitivity: 'base' }) === 0).sort((a, b) => b.accessLevel - a.accessLevel)[0];
    return (
      allowlistedUsers.indexOf(member.username) === -1 &&
      (accessForUser === undefined ||
        (member.access_level > accessForUser.accessLevel &&
          accessForUser.accessLevel !== maintainerPermissionsLevel)) &&
      !this.isBot(member.username, projectIDs)
    );
  }

  /**
   * Iterates over the projects cache and cleans out the users and keeps bots for build operations. Skips over projects
   * outside the scope of the designated root group to avoid over processing groups.
   */
  async cleanupProjects() {
    this.gitlabWrapper.getAllProjects().forEach(async p => {
      // don't process projects outside target namespace
      if (!this.withinNamespace(p.namespace.full_path)) {
        return;
      }

      // get the group of the project and clean it up
      const group = await this.getGroup(p.namespace.full_path);
      if (group !== null) {
        this.cleanUpProjectUsers(p, ...group.projectTargets);
      } else {
        this.logger.info(`Skipping processing of project '${p.name}'`);
      }
    });
  }

  /**
   * Removes any non-owner user that isn't a bot from projects. Membership is managed at the group level, not the direct
   * project level.
   *
   * @param project the Gitlab project to sanitize
   * @param projectIDs the Eclipse projects that impact the Gitlab project.
   */
  async cleanUpProjectUsers(project: ProjectSchema, ...projectIDs: string[]) {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:cleanUpProjectUsers(project = ${project.id})`);
    }
    const projectMembers = await this.gitlabWrapper.getAllProjectMembers(project.id);
    for (const idx in projectMembers) {
      const member = projectMembers[idx];
      this.logger.verbose(`Checking '${member.username}' for removal on project '${project.namespace.full_path}'(${member.access_level})`);
      // skip bot user or admin users
      if (this.isBot(member.username, projectIDs) || member.access_level === adminPermissionsLevel) {
        continue;
      }
      if (this.config.dryRun) {
        this.logger.debug(`Dryrun flag active, would have removed user '${member.username}' from project '${project.name}'(${project.id})`);
        continue;
      }
      this.logger.info(
        `Removing user '${member.username}' with permissions '${member.access_level}' from project ` + `'${project.name}'(${project.id})`
      );

      await this.gitlabWrapper.removeProjectMember(project.id, member.id)
        .catch(err => {
          if (this.config.verbose) {
            this.logger.error(`${err}`);
          }
          this.logger.error(`Error while removing user '${member.username}' from project '${project.name}'(${project.id})`);
        });
    }
  }

  /**
   * Fetches all private repos and determines whether or not they meet deletion criteria.
   * If they meet the criteria, checks for deletion issues and creates one if it does not already exist or
   * if the ones that do exist were created before the last activity date.
   */
  async managePrivateProjects(): Promise<void> {
    if (this.config.verbose) {
      this.logger.debug('GitlabSync:managePrivateProjects');
    }

    // Check for private projects
    const privateProjects = await this.gitlabWrapper.api.Projects.all({ visibility: 'private' });
    if (privateProjects.length === 0) {
      this.logger.warn('Could not find any private projects');
      return;
    }

    // Filter for any projects under the 'eclipse/' namespace
    const filteredProjects = privateProjects.filter(p => p.owner !== undefined || p.path_with_namespace.startsWith('eclipse/'));

    for (const project of filteredProjects) {
      if (this.isScheduledForDeletion(project.last_activity_at)) {
        this.logger.info(`Project: (id=${project.id}, name=${project.name_with_namespace}') is scheduled for deletion`);

        if (!this.projectHasDeletionIssue(project.id) || !this.hasDeletionNoticeAtOrAfterLastActive(project.id, project.last_activity_at)) {
          await this.createDeletionIssue(project.id);
        } else if (this.config.verbose) {
          this.logger.debug(`Skipping private project: (id=${project.id}, name=${project.path_with_namespace}')`);
        }
      }
    }
  }

  /**
   * Checks if the time since the last project activity is greater than 90 days and is therefore scheduled for deletion.
   * @param lastActivityAt the date of the latest project activity
   * @returns True if greater than or equal to 90 days
   */
  isScheduledForDeletion(lastActivityAt: string): boolean {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:isScheduledforDeletion(date=${lastActivityAt})`);
    }

    const timeSinceActivity = Date.now() - Date.parse(lastActivityAt);
    return timeSinceActivity >= repoExpiryAsMillis;
  }

  /**
   * Fetches the issues assigned to webmaster, then checks if any of their project ids match the given project id.
   * Returns true if any match exists
   * @param projectId the id of the project to check for issues
   * @returns True if deletion issue exists
   */
  projectHasDeletionIssue(projectId: number): boolean {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:projectHasDeletionIssue(projectId=${projectId})`);
    }

    // Check if any deletion issues have been created
    const hasIssue = this.gitlabWrapper.getAllIssues().some(issue => (issue.project_id === projectId));
    if (hasIssue && this.config.verbose) {
      this.logger.debug(`Project with id '${projectId}' has a deletion issue`);
    }

    return hasIssue;
  }

  /**
  * Filters the issue cache for issues that match the given project id.
  * Checks these issues and determines whether or not a deltion notice was issued at the last active date.
  * This flag is used to determine whether another deletion issue should be created.
  * If the latest issue is not a deletion notice, the repo is active, and should not be deleted.
  * @param projectId The project id used to filter issues
  * @param lastActivityAt The project's last activity date
  * @returns true if an issue was created after last activity date
  */
  hasDeletionNoticeAtOrAfterLastActive(projectId: number, lastActivityAt: string): boolean {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:issuedBeforeLastActiveDate(projectId=${projectId}, lastActivityAt=${lastActivityAt})`);
    }

    // Get all issues for this project
    const deletionIssues = this.gitlabWrapper.getAllIssues().filter(issue => (issue.project_id === projectId));
    const lastActivityDate = Date.parse(lastActivityAt);

    // Check if any deletion issues were created since or on the last activity date
    const hasDeletionNoticeAtOrAfterLastActive = deletionIssues.some(a => Date.parse(a.created_at as string) >= lastActivityDate);
    if (hasDeletionNoticeAtOrAfterLastActive && this.config.verbose) {
      this.logger.debug(`Project: (id='${projectId}') latest issue is a deletion notice`);
    }

    return hasDeletionNoticeAtOrAfterLastActive;
  }

  /**
   * Creates a new issue under a given project using the project id. The issue acts as a flag for deletion.
   * @param projectId The project id for which to create the issue
   * @returns the new issue or null
   */
  async createDeletionIssue(projectId: number): Promise<IssueSchema> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:createDeletionIssue(projectId=${projectId})`);
    }

    const issueOptions = {
      title: 'Deletion of this project',
      description: 'This project has been inactive for 90 days and has been flagged for deletion.'
    };

    const newIssue = await this.gitlabWrapper.createIssue(projectId, issueOptions);
    if (newIssue !== null) {
      this.logger.info(`Created issue with id '${newIssue.id}' for project with id '${projectId}'`);
    }

    return newIssue;
  }


  /**
   * Ensures that the user exists within the group with the given access level (no more or less). If a user has too high
   * permissions, the membership is modified to have the given access instead.
   *
   * @param user the user that is being given permissions
   * @param group group that the user should be added to
   * @param perms the permission set to give the user
   * @returns the membership information for the user wrt to this Gitlab group.
   */
  async addUserToAsset(user: SimpleUserSchema, asset: GroupSchema | ProjectSchema, perms: NonAdminAccessLevel)
    : Promise<MemberSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:addUserToAsset(user = ${user?.username}, asset = ${asset.path}, perms = ${perms})`);
    }
    // keep ref to type narrowing check for future checks
    const isGroupRequest = this.isGroup(asset);
    const assetLogName = isGroupRequest ? 'group' : 'project';
    // based on narrowing, get members
    const members = await (isGroupRequest ? this.getGroupMembers(asset) : this.gitlabWrapper.getAllProjectMembers(asset.id, false));
    if (members === null) {
      this.logger.warn(`Could not find any references to ${assetLogName} with ID ${asset.id}`);
      return null;
    }

    // check if user is already present
    let existingMember = members.find(member => member !== undefined &&
      member.username.localeCompare(user.username, undefined, { sensitivity: 'base' }) === 0);
    if (existingMember !== undefined) {
      this.logger.verbose(`User '${user.username}' is already a member of ${asset.name}`);
      if (existingMember.access_level !== perms) {
        // skip if dryrun
        if (this.config.dryRun) {
          this.logger.info(`Dryrun flag active, would have updated user '${existingMember.username}' in ${assetLogName} '${asset.name}'`);
          return null;
        }

        // modify user
        this.logger.info(`Fixing permission level for user '${user.username}' in ${assetLogName} '${asset.name}'`);

        const updatedMember = await (isGroupRequest ? this.gitlabWrapper.editGroupMember(asset.id, user.id, perms)
          : this.gitlabWrapper.editProjectMember(asset.id, user.id, perms));
        if (updatedMember === null) {
          this.logger.warn(`Error while fixing permission level for user '${user.username}' in ${assetLogName} '${asset.name}'`);
          return null;
        }
        existingMember = updatedMember;
      }

      // return a copy of the updated user
      return existingMember;
    }

    // check if dry run before updating
    if (this.config.dryRun) {
      this.logger.info(
        `Dryrun flag active, would have added user '${user.username}' to ${assetLogName} '${asset.name}' with access level '${perms}'`
      );
      return null;
    }

    this.logger.info(`Adding '${user.username}' to '${asset.name}' ${assetLogName}`);

    // add member to asset, track, and return a copy
    const newMember = await (isGroupRequest ? this.gitlabWrapper.addGroupMember(asset.id, user.id, perms)
      : this.gitlabWrapper.addProjectMember(asset.id, user.id, perms));
    if (newMember === null) {
      this.logger.warn(`Error while adding '${user.username}' to '${asset.name}' ${assetLogName}`);
      return null;
    }
    return newMember;
  }

  /**
   * Retrieves a Gitlab user object for the given Eclipse user given their username and access URL. If the
   * user does not yet exist, a new user is created, cached, and returned for use.
   *
   * @param uname the Eclipse username of user to retrieve from Gitlab
   * @param url the Eclipse user access URL
   * @returns the gitlab user, or null if it can't be found or created.
   */
  async getUser(uname: string, url: string): Promise<SimpleUserSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getUser(uname = ${uname}, url = ${url})`);
    }
    if (url === undefined || url === '') {
      this.logger.error(`Cannot fetch user information for user '${uname}' with no set URL`);
      return null;
    }

    let u = await this.gitlabWrapper.getNamedUser(uname);
    if (u === null) {
      // retrieve user data
      const data = await this.eApi.eclipseUser(uname);
      if (data === null) {
        this.logger.error(`Cannot create linked user account for '${uname}', no external data found`);
        return null;
      }
      this.logger.verbose(`Creating new user with name '${uname}'`);
      const opts = {
        username: uname,
        password: v4(),
        force_random_password: true,
        name: `${data.first_name} ${data.last_name}`,
        email: data.mail,
        extern_uid: data.uid,
        provider: this.config.provider,
        skip_confirmation: true,
      };
      // check if dry run before creating new user
      if (this.config.dryRun) {
        this.logger.info(`Dryrun flag active, would have created new user '${uname}' with options ${JSON.stringify(opts)}`);
        return null;
      }

      // if verbose, display information being used to generate user
      if (this.config.verbose) {
        // copy the object and redact the password for security
        const optLog = JSON.parse(JSON.stringify(opts));
        optLog.password = 'redacted';
        this.logger.debug(`Creating user with options: ${JSON.stringify(optLog)}`);
      }

      u = await this.gitlabWrapper.createUser(opts);

      if (u === null) {
        this.logger.warn(`Error while creating user '${uname}'`);
        return null;
      }
    }
    return u;
  }

  /**
   * Retrieves the list of direct members for a given group, ignoring inherited users.
   *
   * @param group the Gitlab group to retrieve members for
   * @returns a list of Gitlab group members for the given group, or null if there is an error while fetching.
   */
  async getGroupMembers(group: GroupSchema): Promise<MemberSchema[] | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getGroupMembers(group = ${group?.full_path})`);
    }

    const members = await this.gitlabWrapper.getGroupMembers(group.id);
    if (members === null) {
      this.logger.warn(`Unable to find group members for group with ID '${group.id}'`);
      return null;
    }

    return [...members];
  }

  /** HELPERS */

  /**
  * Adds a group to the nested group cache, using the groups full_path property to discover how to insert the
  * entry into the nested cache. Any cache nodes that do not exist yet will be created as the group is inserted.
  *
  * @param g the Gitlab group that is being inserted into the group cache.
  * @returns the group cache entry for the cached gitlab group
  */
  addGroup(g: GroupSchema): GroupCache {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:addGroup(g = ${g.id})`);
    }
    const namespace = g.full_path.toLowerCase();
    // split into group namespace paths (eclipse/sample/group.path into ['eclipse','sample','group.path'])
    const namespaceParts = namespace.split('/');
    return this.tunnelAndInsert(namespaceParts, g, this.nestedGroupCache);
  }

  /**
  * Retrieves the group for the given namespace path. This namespace path should be formatted such that each group path is separated
  * by a slash, eg. eclipse/sample/group. This will be split and used to iterate through the nested cache, returning the group once
  * each namespace path part is used.
  *
  * @param namespace the full path of the group namespace to retrieve.
  * @returns the group cache for the group indicated by the namespace string, or null if there is no matching group.
  */
  async getGroup(namespace: string, rootGroup: string = this.config.rootGroup): Promise<GroupCache | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getCachedGroup(${namespace})`);
    }

    const normalizedNamespace = namespace.toLowerCase();
    if (!this.withinNamespace(normalizedNamespace, rootGroup)) {
      this.logger.info(`Returning null for ${namespace} as it is outside of the root group ${rootGroup}`);
      return null;
    }

    return this.tunnelAndRetrieve(normalizedNamespace.split('/'), this.nestedGroupCache);
  }


  /**
  * Generate the nested group cache using the raw Gitlab group definitions.
  * @param rawGroups
  */
  generateGroupsCache(rawGroups: GroupSchema[]): void {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:generateGroupsCache(projects = count->${rawGroups.length})`);
    }
    // create initial cache container
    this.nestedGroupCache = {
      _self: null,
      projectTargets: [],
      children: {},
    };

    // iterate through groups and insert into the nested cache
    for (const element of rawGroups) {
      this.addGroup(element);
    }
  }

  /**
   * HELPERS
   */

  /**
  * Recursive function for inserting groups into the nested group cache. Will tunnel through cache, creating
  * entries as necessary before inserting the group at the nesting level representing the final group in the
  * full path of the group.
  *
  * @param namespaceParts the parts of the full_path left to process for group nesting
  * @param g  the group to be inserted into the group cache.
  * @param parent the parent level for the current level of insertion
  * @returns the group cache entry for the group once inserted.
  */
  tunnelAndInsert(namespaceParts: string[], g: GroupSchema, parent: GroupCache): GroupCache {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:tunnelAndInsert(namespaceParts = '${namespaceParts}', g = ${g.id})`);
    }

    // get the next level cache if it exists, creating it if it doesn't
    let child = parent.children[namespaceParts[0]];
    if (child === undefined) {
      child = {
        _self: null,
        projectTargets: [],
        children: {},
      };
      parent.children[namespaceParts[0]] = child;
    }
    // check if we should continue tunneling or insert and finish processing
    if (namespaceParts.length > 1) {
      return this.tunnelAndInsert(namespaceParts.slice(1, namespaceParts.length), g, child);
    }
    child._self = g;
    return child;
  }

  /**
  * Recursive access to the nested group cache. Retrieves the group described by the namespace parts and returns
  * it, returning null if it can't be found.
  *
  * @param namespaceParts the full path for a group namespace split into parts
  * @param parent the parent to search through for the next part of the recursive call.
  * @returns The group cache for the designated group, or null if it can't be found.
  */
  async tunnelAndRetrieve(namespaceParts: string[], parent: GroupCache): Promise<GroupCache | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:tunnelAndRetrieve(namespaceParts = '${namespaceParts}')`);
    }

    let child = parent.children[namespaceParts[0]];
    if (child === undefined) {
      // attempt to create the new group
      const newGroup = await this.createMissingGroup(namespaceParts[0], parent._self);
      if (newGroup === null) {
        this.logger.warn(`Could not create missing group with name '${namespaceParts[0]}' in group with path '${parent._self.full_path}'`);
        return null;
      }

      // insert the new child group into the cache and continue
      child = this.tunnelAndInsert(newGroup.full_path.split('/'), newGroup, this.getRootGroup());
    }

    // check if we should continue tunneling or insert and finish processing
    if (namespaceParts.length > 1) {
      return this.tunnelAndRetrieve(namespaceParts.slice(1, namespaceParts.length), child);
    }
    return child;
  }

  /**
  * @returns the root group cache for the current sync operation if it exists. If missing, the script ends processing.
  */
  getRootGroup(endProcessing = true): GroupCache {
    const rootGroupCache = this.nestedGroupCache.children[this.config.rootGroup];
    if (rootGroupCache === undefined) {
      if (endProcessing) {
        this.logger.error(`Could not find root group '${this.config.rootGroup}' for group caching, exiting`);
        process.exit(1);
      }
      throw new Error(`Could not find root group '${this.config.rootGroup}' for root group fetch`);
    }
    return rootGroupCache;
  }

  /**
  * Used to create missing groups in the Gitlab instance. Does not insert into the nest cache as this should only be
  * called from said cache. This method does not support creating root level groups.
  *
  * @param name the name of the group to create
  * @param parent the group that this group belongs to
  * @returns the new group schema once the call finishes
  */
  async createMissingGroup(name: string, parent: GroupSchema): Promise<GroupSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:createMissingGroup(name = ${name}, parent = ${parent.id})`);
    }

    // default options for creating new group
    const opts = {
      project_creation_level: 'maintainer',
      visibility: 'public' as ProjectVisibility,
      request_access_enabled: false,
      parent_id: parent.id,
    };
    if (this.config.dryRun) {
      this.logger.debug(`Dryrun flag active, would have added group with options: ${opts}`);
      return null;
    }

    this.logger.info(`Creating missing group '${name}' in namespace '${parent.full_path} (${parent.id})'`);
    return this.gitlabWrapper.createGroup(name, name, opts);
  }

  /**
   * Adapt interest groups into synthetic projects. These adapted projects can be used in downstream calls as if they
   * are projects for sync operations. If the interest group is in the terminated state, do not include any users. This should allow for
   * simple clearing of users within the interest group.
   *
   * @param interestGroups the groups to be adapted
   * @returns the list of adapted projects
   */
  adaptInterestGroupsToProject(interestGroups: InterestGroup[]): EclipseProject[] {
    return interestGroups
      .filter(ig => ig.state.localeCompare('pre-proposed') !== 0)
      .map(ig => {
        return {
          project_id: ig.project_id,
          name: ig.title,
          logo: ig.logo,
          state: ig.state,
          summary: ig.description.summary,
          short_project_id: ig.project_id.substring(ig.project_id.lastIndexOf('.') + 1),
          gitlab: ig.gitlab,
          project_leads: 'terminated'.localeCompare(ig.state, undefined, { sensitivity: 'base' }) !== 0 ? ig.leads : [],
          committers: 'terminated'.localeCompare(ig.state, undefined, { sensitivity: 'base' }) !== 0 ? ig.participants : [],
          contributors: [],
          gerrit_repos: [],
          github_repos: [],
          website_repo: [],
          releases: [],
          spec_project_working_group: [],
          working_groups: [],
          tags: [],
          top_level_project: '',
          url: '',
          website_url: '',
        } as EclipseProject;
      });
  }

  /**
   * Gets list of users with access permissions for the given Eclipse project.
   *
   * @param project the Eclipse project to parse user entries for
   * @returns the list of user access permissions and entity access URL.
   */
  getUserList(project: EclipseProject): Array<EclipseUserAccess> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getUserList(project = ${project?.project_id})`);
    }
    if (project === undefined) {
      return [];
    }
    const l: Record<string, EclipseUserAccess> = {};
    // add the contributors with reporter access
    project.contributors.forEach(v => {
      l[v.username] = {
        username: v.username,
        url: v.url,
        accessLevel: 20,
        projectNamespace: project.gitlab.project_group,
      };
    });
    // add the committers with developer access
    project.committers.forEach(v => {
      l[v.username] = {
        username: v.username,
        url: v.url,
        accessLevel: 30,
        projectNamespace: project.gitlab.project_group,
      };
    });
    // add the project leads not yet tracked with reporter access
    project.project_leads.forEach(v => {
      l[v.username] = {
        username: v.username,
        url: v.url,
        accessLevel: 40,
        projectNamespace: project.gitlab.project_group,
      };
    });
    // handle overrides and exclusions
    Object.keys(l).forEach(un => {
      let override = this.upo.checkUserForOverride(un, project.project_id, 'GITLAB');
      if (override === undefined) {
        // do not change as there is no current override
        return;
      } else if (override === 'blocked') {
        // blocked from access, remove from userlist
        this.logger.info(`User ${un} marked as excluded from run, removing from final userlist`);
        delete l[un];
      } else {
        override = parseInt(override, 10);
        // otherwise, we get a permission back. Set it to the current user object
        this.logger.debug(`User ${un} marked as overriden permissions of ${override} in project ${project.project_id}`);
        l[un].accessLevel = override as NonAdminAccessLevel;
      }
    });

    // add the bots with developer access
    const botList = this.botsCache[project.project_id];
    if (botList !== undefined && botList.length === 0) {
      botList.forEach(v => {
        l[v] = {
          username: v,
          url: '',
          accessLevel: 40,
          projectNamespace: project.gitlab.project_group,
        };
      });
    }
    return Object.values(l);
  }

  /**
   * Sanitizes and normalizes strings for use in creating/accessing groups.
   *
   * @param pid the project ID to normalize
   * @returns normalized group name for value.
   */
  sanitizeGroupName(pid: string): string {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:sanitizeGroupName(pid = ${pid})`);
    }
    if (pid !== undefined) {
      return pid.toLowerCase().replace(/[^\s\da-zA-Z-.]/g, '-');
    }
    return '';
  }

  /**
   * Using a few known project-only fields, check if the ambiguous reference is a group or project.
   *
   * @param asset ambiguous asset that could be a projet or group asset.
   * @returns true if the passed asset is a GroupSchema, otherwise false.
   */
  isGroup(asset: GroupSchema | ProjectSchema): asset is GroupSchema {
    return (<ProjectSchema>asset).ssh_url_to_repo === undefined && (<ProjectSchema>asset).star_count === undefined;
  }

  /**
   * Checks whether a user is a bot for the given projects.
   *
   * @param uname potential bot username
   * @param projectIDs the projects that the user could be a bot for.
   * @returns true if the user is a designated bot for the projects, otherwise false.
   */
  isBot(uname: string, projectIDs: string[]): boolean {
    return projectIDs.some(v => this.botsCache[v] !== undefined && this.botsCache[v].indexOf(uname) !== -1);
  }

  /**
   * Check to ensure that a given namespace is within the target group namespace.
   *
   * @param namespace the namespace to check
   * @returns true if the namespace is under the configured root group, false otherwise.
   */
  withinNamespace(namespace: string, rootGroup: string = this.config.rootGroup): boolean {
    return namespace.startsWith(rootGroup + '/') || namespace === rootGroup;
  }
}
