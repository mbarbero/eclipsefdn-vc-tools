apiVersion: batch/v1
kind: CronJob
metadata:
  name: eclipsefdn-gitlab-sync
  namespace: foundation-internal-webdev-apps
  labels:
    job: gitlab-sync
spec:
  schedule: "5 */2 * * *"
  # if it is time for a new job run and the previous job run hasn’t finished yet, the cron job skips the new job run
  concurrencyPolicy: Forbid
  jobTemplate:
    spec:
      # Deadline is 3h to give some wiggle room, this shouldn't block more than 1 run at most
      activeDeadlineSeconds: 21600
      template:
        spec:
          affinity:
            nodeAffinity:
              preferredDuringSchedulingIgnoredDuringExecution:
              - preference:
                  matchExpressions:
                  - key: speed
                    operator: NotIn
                    values:
                    - fast
                weight: 1
          containers:
          - name: eclipsefdn-gitlab-sync
            image: eclipsefdn/eclipsefdn-gitlab-sync:latest
            imagePullPolicy: IfNotPresent
            args:
            - /bin/sh
            - -c
            - npm run lab-sync -- --verbose=true --tls-min-v1.0 --host=https://gitlab.eclipse.org 2>&1 | tee -a /app/logs/gitlab-stdout-$(date +%Y-%m-%d).log
            volumeMounts:
            - name: logs
              mountPath: /app/logs
            - name: cache
              mountPath: /app/.cache
            - name: overrides-excludes
              mountPath: /run/secrets/permissions.json
              subPath: permissions.json
            - name: gitlab-sync-secrets
              mountPath: "/run/secrets/access-token"
              readOnly: true
              # workaround https://github.com/kubernetes/kubernetes/issues/65835
              subPath: access-token
            - name: gitlab-sync-secrets-oauth
              mountPath: "/run/secrets/eclipse-oauth-config"
              readOnly: true
              # workaround https://github.com/kubernetes/kubernetes/issues/65835
              subPath: eclipse-oauth-config
          restartPolicy: Never
          volumes:
          - name: logs
            persistentVolumeClaim:
              claimName: gitlab-sync-logs
          - name: cache
            emptyDir: {}
          - name: overrides-excludes
            configMap:
              name: ef-version-control-overrides
              items:
              - key: permissions.json
                path: permissions.json
          - name: gitlab-sync-secrets
            secret:
              secretName: gitlab-sync-secrets
              # workaround https://github.com/kubernetes/kubernetes/issues/65835
              items:
              - key: access-token
                path: access-token
          - name: gitlab-sync-secrets-oauth
            secret:
              secretName: gitlab-sync-secrets-oauth
              # workaround https://github.com/kubernetes/kubernetes/issues/65835
              items:
              - key: eclipse-oauth-config
                path: eclipse-oauth-config
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: gitlab-sync-logs
  annotations:
    volume.beta.kubernetes.io/mount-options: rw,nfsvers=3,noexec
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  claimRef:
    namespace: foundation-internal-webdev-apps
    name: gitlab-sync-logs
  nfs:
    server: nfsmaster
    path: /opt/export/eclipsefdn-gitlab-sync/logs
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: gitlab-sync-logs
  namespace: foundation-internal-webdev-apps
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
