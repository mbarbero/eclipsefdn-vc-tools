FROM node:lts-hydrogen
WORKDIR /app
ARG VERBOSE_VAL=false
ENV VERBOSE_VAL ${VERBOSE_VAL:-false}
ARG DRYRUN=false
ENV DRYRUN ${DRYRUN:-false}
ARG DRYRUN_DELETION=false
ENV DRYRUN_DELETION ${DRYRUN_DELETION:-false}

## Copy over required script files
COPY src src/

## Set up the cache for the run outside of the root-owned namespace
RUN mkdir .npm
RUN npm config set cache .npm --global

## Copy NPM configs and install dependencies
COPY package*.json ./
RUN npm ci --ignore-scripts

## run the script
CMD npm start -- --verbose="$VERBOSE_VAL" --dryrun="$DRYRUN" --deletionDryRun="$DRYRUN_DELETION" --tls-min-v1.0