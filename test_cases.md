# Test cases 

- Groups with a namespace root that starts with the target group aren't affected by cleanup. ([#214](https://gitlab.eclipse.org/eclipsefdn/it/webdev/eclipsefdn-vc-tools/-/issues/214))
  - Check for this would be having eclipse and eclipsefdn as root groups, and to make sure that both groups and projects under the eclipsefdn root are unaffected by the cleanup. We should add users to both the group and project that aren't owners to test.